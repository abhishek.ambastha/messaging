from kafka import KafkaProducer

producer = KafkaProducer(bootstrap_servers=['localhost:9092'])

producer.send('sample', b'Hello World')
producer.send('sample', key=b'message-key', value=b'actual message')
producer.flush()
